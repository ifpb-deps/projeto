# Comande-se #
### Sistema de gerenciamento de comandas ###
Desenvolvido por alunos do IFPB para a disciplina de DEPS, em 2017.2

### Tecnologias utilizadas? ###

* PHP
* CodeIgniter
* SourceTree
* Bitbucket
* Google Drive

### Desenvolvimento ###

Guia de ferramentas:
[Arquivo no GDrive (DEPS > Artefatos > 00-Templates > 04 - Implementação)](https://drive.google.com/open?id=1nTyRreEkIuVxHx5BU_din22o48BRga62sNY1aeKdkvc)

### Testes ###

* XXX
* YYY
* ZZZ
